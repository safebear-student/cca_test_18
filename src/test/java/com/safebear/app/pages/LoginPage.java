package com.safebear.app.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {

    WebDriver driver;

    @FindBy(id = "myid")
    WebElement username_field;

    @FindBy(id = "mypass")
    WebElement password_field;

    public LoginPage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }
    public boolean checkCorrectPage(){
        return driver.getTitle().startsWith("Sign");
    }

    public boolean login(UserPage userPage, String username, String password){
        this.username_field.sendKeys(username);
        this.password_field.sendKeys(password);
        this.password_field.submit();
        return userPage.checkCorrectPage();
    }
}
