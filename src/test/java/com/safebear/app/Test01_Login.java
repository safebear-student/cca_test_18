package com.safebear.app;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class Test01_Login extends BaseTest {
    @Test
    public void testLogin(){
        // Step 1 confirm we're on the Welcome Page
        assertTrue(welcomePage.checkCorrectPage());
        // Step 2 click on the Login link
        assertTrue(welcomePage.clickOnLogin(this.loginPage));
        // Step 3 Login with valid credentials
        assertTrue(loginPage.login(this.userPage,"testuser","testing"));
    }

}
